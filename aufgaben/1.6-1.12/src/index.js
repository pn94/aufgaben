import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hyvä: 0,
      neutraali: 0,
      huono: 0,
      keskiarvo: 0,
      kaikki: 0
    }
  }
  klikHyva = () => {
    this.setState({
      hyvä: this.state.hyvä + 1,
      kaikki: this.state.kaikki + 1
    })
  }
  klikNeutraali = () => {
    this.setState({
      neutraali: this.state.neutraali + 1,
      kaikki: this.state.kaikki + 1
    })
  }
  klikHuono = () => {
    this.setState({
      huono: this.state.huono + 1,
      kaikki: this.state.kaikki + 1
    })
  }
  keskiArvo = () => {
    var ka = ((this.state.hyvä - this.state.huono) / (this.state.hyvä + this.state.neutraali + this.state.huono))
    return ka.toFixed(2)
  }
  positiivisia = () => {
    var pos = (this.state.hyvä / (this.state.huono + this.state.neutraali + this.state.hyvä)) * 100
    return pos.toFixed(1)
  }

  render() {
    const historia = () => {
      if (this.state.kaikki > 0) {
        return (<div>
          <table>
            <tr>
          <td>hyvä {this.state.hyvä}</td>
          </tr>
          <tr>
          <td>neutraali {this.state.neutraali}</td>
          </tr>
          <tr>
          <td>huono {this.state.huono}</td>
          </tr>
          <tr>
          <td>keskiarvojaaa {this.keskiArvo()}</td>
          </tr>
          <tr>
          <td>positiivisia {this.positiivisia()}%</td>
          </tr>
          </table>
        </div>)
      } else if (this.state.kaikki === 0) {
        return (<div>
          <p>Ei yhtään palautetta annettu</p>
        </div>)

      }


  }

  return (<div>
    <h2>Anna palautetta</h2>
    <button onClick={this.klikHyva}>hyvä</button>
    <button onClick={this.klikNeutraali}>neutraali</button>
    <button onClick={this.klikHuono}>huono</button>
    <h2>Statistiikka</h2>
    <p>{historia()}</p>

  </div>)
}
}

ReactDOM.render(<App/>, document.getElementById('root'));;
